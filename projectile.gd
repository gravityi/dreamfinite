extends Spatial


var target

func _process(delta):
	look_at(target.global_transform.origin, Vector3.UP)
	translation -= transform.basis.z * 20 * delta
	if transform.origin.distance_to(target.transform.origin) < 2:
		target.destroy()
		queue_free()
