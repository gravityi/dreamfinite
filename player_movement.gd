extends KinematicBody

export var move_speed = 10
const Projectile = preload("res://Projectile.tscn")

var horizontal = 0
var vertical = 0
var astral_mode = false

func _process(delta):
	if Input.is_action_just_pressed("ui_up"):
		vertical += 1
	if Input.is_action_just_pressed("ui_down"):
		vertical -= 1
	if Input.is_action_just_pressed("ui_left"):
		horizontal += 1
	if Input.is_action_just_pressed("ui_right"):
		horizontal -= 1

	if Input.is_action_just_released("ui_up"):
		vertical -= 1
	if Input.is_action_just_released("ui_down"):
		vertical += 1
	if Input.is_action_just_released("ui_left"):
		horizontal -= 1
	if Input.is_action_just_released("ui_right"):
		horizontal += 1
	
	if Input.is_action_just_pressed("ui_accept") and !DialogueSystem.playing_dialogue:
		astral_mode = !astral_mode
		if astral_mode: $"../Camera".environment = load("res://AstralWorld.tres")
		else: $"../Camera".environment = load("res://NormalWorld.tres")
		Globals.player_in_astral = astral_mode
	
	#Movement and Collision Handling
	var motion = Vector3(horizontal, 0, vertical).normalized()
	if !(Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		vertical = 0
		horizontal = 0
		motion = Vector3.ZERO
	else:
		motion = -Vector3(horizontal, 0, vertical).normalized()
		if transform.origin != (transform.origin - motion.rotated(Vector3.UP, deg2rad(45))):
			# THE LINE (I don't understand Quaternions exactly, I just know how to make them work.)
			transform.basis = Basis(Quat(transform.basis).slerp(Quat(transform.looking_at(transform.origin - motion.rotated(Vector3.UP, deg2rad(45)), Vector3.UP).basis), 0.1))
	move_and_collide(motion.rotated(Vector3.UP, deg2rad(45)) * delta * move_speed)

func shootProjectile(target):
	look_at(-Vector3(target.transform.origin.x, transform.origin.y, target.transform.origin.z), Vector3.UP)
	var NewProjectile = Projectile.instance()
	NewProjectile.transform.origin = transform.origin
	NewProjectile.target = target
	get_parent().get_parent().add_child(NewProjectile)
