shader_type spatial;
render_mode world_vertex_coords, diffuse_lambert, specular_disabled;

void vertex(){
	float curvature = 0.001;
	vec3 camPos = (CAMERA_MATRIX * vec4(0.0, 0.0, 0.0, 1.0)).xyz;
	VERTEX -= camPos.xyz;
	VERTEX.y -= (pow(VERTEX.z, 2) + pow(VERTEX.x, 2)) * curvature;
	VERTEX = (VERTEX).xyz;
}