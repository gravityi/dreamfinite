extends Spatial

export var screenwipe = false
var spawned_words = []
var spawned_enemies = {}
var astral_words = []
var astral_enemies = {}
const EnemyNode = preload("res://Enemy.tscn")
var word_data
var spawned = false

onready var Player = $"../../Player/PlayerBody"

func _load_word_data():
	randomize()
	var file = File.new()
	file.open("res://data/enemywords.json", File.READ)
	word_data = parse_json(file.get_as_text())

func define_word(type_id : String, astral : bool, random : bool):
	var word_list = word_data[type_id]
	var random_word = word_list[randi() % word_list.size()]
	# Return random word if intended
	if !astral:
		if random or spawned_words.empty(): return random_word 
		else:
			var possible_words = word_list.duplicate(true)
			for word in spawned_words: if word in possible_words: possible_words.erase(word)
			# Return any other word if possible
			if !possible_words.empty(): return possible_words[randi() % possible_words.size()]
			# Otherwise, return random word
			else: return random_word
	else:
		if random or astral_words.empty(): return random_word 
		else:
			var possible_words = word_list.duplicate(true)
			for word in astral_words: if word in possible_words: possible_words.erase(word)
			# Return any other word if possible
			if !possible_words.empty(): return possible_words[randi() % possible_words.size()]
			# Otherwise, return random word
			else: return random_word

func spawn_enemy(position : Vector3, type_id : String, astral = false, random : bool = false):
	var NewEnemy = EnemyNode.instance()
	var word = define_word(type_id, astral, random)
	NewEnemy.translation = position
	NewEnemy.word = word
	NewEnemy.astral = astral
	if !astral:
		if !word in spawned_words: spawned_words.append(word)
		if !spawned_enemies.has(word): spawned_enemies[word] = [NewEnemy]
		else: spawned_enemies[word].append(NewEnemy)
	else:
		if !word in astral_words: astral_words.append(word)
		if !astral_enemies.has(word): astral_enemies[word] = [NewEnemy]
		else: astral_enemies[word].append(NewEnemy)
	add_child(NewEnemy)

#Just a generic ascending sort function
func distance_sort(a, b):
	if a[1] < b[1]:
		return true
	return false

func sort_enemies_by_distance():
	for word in spawned_words:
		var enemy_list = []
		for enemy in spawned_enemies[word]:
			enemy_list.append([enemy, enemy.transform.origin.distance_to(Player.transform.origin)])
			if !enemy.closest: enemy.char_index = 0
			enemy.closest = false
		enemy_list.sort_custom(self, "distance_sort")
		enemy_list[0][0].closest = true
	for word in astral_words:
		var enemy_list = []
		for enemy in astral_enemies[word]:
			enemy_list.append([enemy, enemy.transform.origin.distance_to(Player.transform.origin)])
			if !enemy.closest: enemy.char_index = 0
			enemy.closest = false
		enemy_list.sort_custom(self, "distance_sort")
		enemy_list[0][0].closest = true

#Cleans up the data structures which contains info about spawned enemies and words.
func clean_up(caller):
	if !caller.astral:
		spawned_enemies[caller.word].erase(caller)
		if spawned_enemies[caller.word].empty():
			spawned_words.erase(caller.word)
			spawned_enemies.erase(caller.word)
	else:
		astral_enemies[caller.word].erase(caller)
		if astral_enemies[caller.word].empty():
			astral_words.erase(caller.word)
			astral_enemies.erase(caller.word)

#Utility functions
func clear_screen():
	for word in spawned_words:
		for enemy in spawned_enemies[word]:
			enemy.destroy()
	spawned_words.clear()
	spawned_enemies.clear()

func clear_astral():
	for word in astral_words:
		for enemy in astral_enemies[word]:
			enemy.destroy()
	astral_words.clear()
	astral_enemies.clear()

func reset_enemies():
	spawn_enemy(Vector3(5,1,5), "skeleton")
	spawn_enemy(Vector3(-5,1,5), "skeleton")
	spawn_enemy(Vector3(5,1,-5), "skeleton")
	spawn_enemy(Vector3(-5,1,-5), "skeleton")
	spawn_enemy(Vector3(-5,1,-30), "skeleton")
	spawn_enemy(Vector3(10,1,10), "skeleton", true)
	spawn_enemy(Vector3(-10,1,10), "skeleton", true)
	spawn_enemy(Vector3(10,1,-10), "skeleton", true)
	spawn_enemy(Vector3(-10,1,-10), "skeleton", true)
	spawn_enemy(Vector3(-10,1,-30), "skeleton", true)

#Main functions
func _ready():
	_load_word_data()
	yield(get_tree().create_timer(1), "timeout")

func _process(_delta):
	screenwipe = $"multikill".pressed
	if screenwipe: for word in spawned_words: for enemy in spawned_enemies[word]: enemy.closest = true
	else: sort_enemies_by_distance()

func _on_SpawnRadius_body_entered(body):
	if body.is_in_group("player") and !spawned:
		reset_enemies()
		spawned = true
