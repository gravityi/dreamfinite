extends Camera


onready var PlayerBody = $"../PlayerBody"

func _process(_delta):
	transform.origin = PlayerBody.transform.origin + Vector3(10, 15, 10)
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
