class_name Enemy
extends Spatial

signal word_typed

const Emitter = preload("res://EnemyEmitter.tscn")

var character
var typed_word = ""
var char_index = 0
var word = "SKELETON"
var closest = true
var astral = false

onready var TextManager = $"TextManager"
onready var Player = $"../../Player/PlayerBody"

func _ready():
	TextManager.show_text()
	$"AnimationPlayer".play_backwards("fullyTyped")
# warning-ignore:return_value_discarded
	connect("word_typed", Player, "shootProjectile", [self])
# warning-ignore:return_value_discarded
	connect("word_typed", get_parent(), "clean_up", [self])

func _input(event):
	if event is InputEventKey:
		if !event.is_echo() and Input.is_key_pressed(event.scancode) and char_index < word.length() and closest and !$"AnimationPlayer".current_animation == "fullyTyped" and astral == Globals.player_in_astral:
			character = char(event.scancode)
			if str(character) == word[char_index].to_upper():
				typed_word += character
				char_index += 1
				# Execute if word completed
				if char_index == word.length():
					emit_signal("word_typed")
				TextManager.show_text()
				$"AnimationPlayer".play("letterTyped")
			# Execute if user did not type correctly
			elif character.to_lower() in "abcdefghijklmnopqrstuvwxyz":
				char_index = 0
				typed_word = ""
				TextManager.show_text()

func destroy():
	$"AnimationPlayer".play("fullyTyped")
	createEmitter()
	char_index = 0
	typed_word = ""
	yield($"AnimationPlayer", "animation_finished")
	queue_free()

func createEmitter():
	var NewEmitter = Emitter.instance()
	NewEmitter.emitting = true
	NewEmitter.transform.origin = transform.origin
	get_parent().add_child(NewEmitter)

func change_material():
	if Globals.player_in_astral and astral:
		$"MeshInstance".material_override = load("res://AstralEnemyAstralMaterial.tres")
	elif Globals.player_in_astral and !astral:
		$"MeshInstance".material_override = load("res://NormalEnemyAstralMaterial.tres")
	elif !Globals.player_in_astral and astral:
		$"MeshInstance".material_override = load("res://AstralEnemyNormalMaterial.tres")
	else:
		$"MeshInstance".material_override = null

func _process(delta):
	change_material()
		
