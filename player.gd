extends KinematicBody


var front = 0
var side = 0

onready var camera = $"Camera"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_up"):
		front -= 1

	if Input.is_action_just_pressed("ui_down"):
		front += 1

	if Input.is_action_just_pressed("ui_left"):
		side -= 1

	if Input.is_action_just_pressed("ui_right"):
		side += 1

	if Input.is_action_just_released("ui_up"):
		front += 1
	if Input.is_action_just_released("ui_down"):
		front -= 1
	if Input.is_action_just_released("ui_left"):
		side += 1
	if Input.is_action_just_released("ui_right"):
		side -= 1
	
	if !(Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		front = 0
		side = 0
	
	var motion2d = Vector2(side, front).normalized() * 10
	var motion = Vector3(motion2d.x, 0, motion2d.y).rotated(Vector3.UP, camera.rotation.y)
# warning-ignore:return_value_discarded
	move_and_slide(motion, Vector3.UP)
